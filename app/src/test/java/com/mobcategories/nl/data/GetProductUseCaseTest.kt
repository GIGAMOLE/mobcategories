package com.mobcategories.nl.data

import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.ProductRepository
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.domain.callLocal
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.configuration.injection.MockInjection

@ExperimentalCoroutinesApi
class GetProductUseCaseTest {

    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var productRepository: ProductRepository

    private lateinit var getProductUseCase: GetProductUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getProductUseCase = GetProductUseCase(productRepository, dispatcher)
    }

    @Test
    fun `when get product return result then success`() = runBlockingTest {
        val id = "id"
        val categoryId = "categoryId"

        val product = Product(id = id, categoryId = categoryId)

        `when`(productRepository.getProductLocal(id, categoryId)).thenReturn(product)

        val result = getProductUseCase.getProduct(id, categoryId)

        verify(productRepository).getProductLocal(id, categoryId)
        assertEquals(Result.Success(product), result)
    }
}
