package com.mobcategories.nl.data

import com.mobcategories.nl.data.remote.ImageUrlMapper
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.domain.callLocal
import com.mobcategories.nl.domain.callRemote
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.internal.matchers.Null
import java.io.IOException
import java.lang.NullPointerException

@ExperimentalCoroutinesApi
class ImageUrlMapperTest {

    private val baseUrl = "https://base.url"
    private val imageUrlMapper = ImageUrlMapper(baseUrl)

    @Test
    fun `when call map return proper image url`() = runBlockingTest {
        val imageUrl = "/blabla.jpg"

        val result = imageUrlMapper.map(imageUrl)

        assertEquals(baseUrl + imageUrl, result)
    }
}
