package com.mobcategories.nl.data

import com.mobcategories.nl.data.remote.NetworkChecker
import com.mobcategories.nl.domain.CategoryRepository
import com.mobcategories.nl.domain.GetAllCategoriesUseCase
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.ProductRepository
import com.mobcategories.nl.domain.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class GetAllCategoriesUseCaseTest {

    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var categoryRepository: CategoryRepository

    @Mock
    private lateinit var networkChecker: NetworkChecker

    private lateinit var getAllCategoriesUseCase: GetAllCategoriesUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getAllCategoriesUseCase =
            GetAllCategoriesUseCase(categoryRepository, networkChecker, dispatcher)
    }

    @Test
    fun `when get product all categories remote return result then success`() = runBlockingTest {
        val id1 = "1"
        val id2 = "2"

        val categories = listOf(
            Category(id = id1),
            Category(id = id2)
        )

        `when`(categoryRepository.getAllCategoriesRemote()).thenReturn(categories)
        `when`(networkChecker.isAvailable()).thenReturn(true)

        val result = getAllCategoriesUseCase.getAllCategories()

        verify(categoryRepository).getAllCategoriesRemote()
        assertEquals(Result.Success(categories), result)
    }

    @Test
    fun `when get product all categories local return result then success`() = runBlockingTest {
        val id1 = "1"
        val id2 = "2"

        val categories = listOf(
            Category(id = id1),
            Category(id = id2)
        )

        `when`(categoryRepository.getAllCategoriesLocal()).thenReturn(categories)
        `when`(networkChecker.isAvailable()).thenReturn(false)

        val result = getAllCategoriesUseCase.getAllCategories()

        verify(categoryRepository).getAllCategoriesLocal()
        assertEquals(Result.Success(categories), result)
    }
}
