package com.mobcategories.nl.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mobcategories.nl.data.Category
import com.mobcategories.nl.domain.GetAllCategoriesUseCase
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.model.CategoryMapper
import com.mobcategories.nl.model.CategoryModel
import com.mobcategories.nl.view.list.ListViewModel
import com.mobcategories.nl.view.main.ModelState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.lang.NullPointerException

@ExperimentalCoroutinesApi
class ListViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var getAllCategoriesUseCase: GetAllCategoriesUseCase

    @Mock
    private lateinit var categoryMapper: CategoryMapper

    private lateinit var listViewModel: ListViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        listViewModel = ListViewModel(getAllCategoriesUseCase, categoryMapper)
    }

    @Test
    fun `when get all categories then observe success`() = runBlockingTest {
        val categories = listOf(Category(), Category())
        val categoryModels = listOf(
            CategoryModel("", emptyList()),
            CategoryModel("", emptyList())
        )

        `when`(getAllCategoriesUseCase.getAllCategories()).thenReturn(Result.Success(categories))
        `when`(categoryMapper.map(categories)).thenReturn(categoryModels)

        listViewModel.getAllCategories()

        assertEquals(ModelState.SUCCESS, listViewModel.allCategoriesState.value)
        assertEquals(categoryModels, listViewModel.allCategories.value)
    }

    @Test
    fun `when get all categories then observe empty`() = runBlockingTest {
        val categories = emptyList<Category>()

        `when`(getAllCategoriesUseCase.getAllCategories()).thenReturn(Result.Success(categories))

        listViewModel.getAllCategories()

        assertEquals(ModelState.EMPTY, listViewModel.allCategoriesState.value)
        assertEquals(null, listViewModel.allCategories.value)
    }

    @Test
    fun `when get all categories then observe error`() = runBlockingTest {
        `when`(getAllCategoriesUseCase.getAllCategories()).thenReturn(Result.NetworkError)

        listViewModel.getAllCategories()

        assertEquals(ModelState.NETWORK_ERROR, listViewModel.allCategoriesState.value)
        assertEquals(null, listViewModel.allCategories.value)
    }

    @Test
    fun `when get all categories then observe exception`() = runBlockingTest {
        val nullPointerException = NullPointerException()

        `when`(getAllCategoriesUseCase.getAllCategories()).thenThrow(nullPointerException)

        listViewModel.getAllCategories()

        assertEquals(ModelState.GENERIC_ERROR, listViewModel.allCategoriesState.value)
        assertEquals(null, listViewModel.allCategories.value)
    }
}
