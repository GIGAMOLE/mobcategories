package com.mobcategories.nl.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.ProductRepository
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.domain.callLocal
import com.mobcategories.nl.view.main.MainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.configuration.injection.MockInjection

@ExperimentalCoroutinesApi
class MainViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mainViewModel = MainViewModel()

    @Test
    fun `when trigger on become online then observe success`() = runBlockingTest {
        mainViewModel.triggerOnBecomeOnline()

        assertEquals(Unit, mainViewModel.onBecomeOnline.value)
    }
}
