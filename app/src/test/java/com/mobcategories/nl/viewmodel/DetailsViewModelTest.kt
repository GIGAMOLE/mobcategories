package com.mobcategories.nl.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product
import com.mobcategories.nl.domain.GetAllCategoriesUseCase
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.model.CategoryMapper
import com.mobcategories.nl.model.CategoryModel
import com.mobcategories.nl.model.ProductMapper
import com.mobcategories.nl.model.ProductModel
import com.mobcategories.nl.view.details.DetailsViewModel
import com.mobcategories.nl.view.list.ListViewModel
import com.mobcategories.nl.view.main.ModelState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.*
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.lang.NullPointerException

@ExperimentalCoroutinesApi
class DetailsViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var getProductUseCase: GetProductUseCase

    @Mock
    private lateinit var productMapper: ProductMapper

    private lateinit var detailsViewModel: DetailsViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        detailsViewModel = DetailsViewModel(getProductUseCase, productMapper)
    }

    @Test
    fun `when get all categories then observe success`() = runBlockingTest {
        val id = "id"
        val categoryId = "categoryId"

        val product = Product()
        val productModel = ProductModel(
            id = id,
            categoryId = categoryId,
            name = "",
            imageUrl = "",
            priceFormat = ""
        )

        `when`(getProductUseCase.getProduct(id, categoryId)).thenReturn(Result.Success(product))
        `when`(productMapper.map(product)).thenReturn(productModel)

        detailsViewModel.getProduct(id, categoryId)

        assertEquals(ModelState.SUCCESS, detailsViewModel.productState.value)
        assertEquals(productModel, detailsViewModel.product.value)
    }

    @Test
    fun `when get all categories then observe error`() = runBlockingTest {
        val id = "id"
        val categoryId = "categoryId"

        `when`(getProductUseCase.getProduct(id, categoryId)).thenReturn(Result.DatabaseError)

        detailsViewModel.getProduct(id, categoryId)

        assertEquals(ModelState.DATABASE_ERROR, detailsViewModel.productState.value)
        assertEquals(null, detailsViewModel.product.value)
    }

    @Test
    fun `when get all categories then observe exception`() = runBlockingTest {
        val nullPointerException = NullPointerException()

        val id = "id"
        val categoryId = "categoryId"

        `when`(getProductUseCase.getProduct(id, categoryId)).thenThrow(nullPointerException)

        detailsViewModel.getProduct(id, categoryId)

        assertEquals(ModelState.GENERIC_ERROR, detailsViewModel.productState.value)
        assertEquals(null, detailsViewModel.product.value)
    }
}
