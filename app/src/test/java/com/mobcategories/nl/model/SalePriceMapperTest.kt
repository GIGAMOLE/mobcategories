package com.mobcategories.nl.model

import com.mobcategories.nl.data.SalePrice
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test

@ExperimentalCoroutinesApi
class SalePriceMapperTest {

    private val salePriceMapper = SalePriceMapper()

    @Test
    fun `when call map return proper sale format`() = runBlockingTest {
        val amount = "0.2"
        val currency = "USD"

        val salePrice = SalePrice(amount = amount, currency = currency)

        val result = salePriceMapper.map(salePrice)

        assertEquals("$amount $currency", result)
    }
}
