package com.mobcategories.nl.model

import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product
import com.mobcategories.nl.data.remote.ImageUrlMapper
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.domain.callLocal
import com.mobcategories.nl.domain.callRemote
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.matchers.Null
import java.io.IOException
import java.lang.NullPointerException
import kotlin.concurrent.timer

@ExperimentalCoroutinesApi
class CategoryMapperTest {

    @Mock
    private lateinit var productMapper: ProductMapper

    private lateinit var categoryMapper: CategoryMapper

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        categoryMapper = CategoryMapper(productMapper)
    }

    @Test
    fun `when call map return proper category model`() = runBlockingTest {
        val name = "name"

        val product = Product()
        val category = Category(name = name, products = listOf(product, product))
        val categoryModel = CategoryModel(name = name, products = emptyList())

        val result = categoryMapper.map(category)

        verify(productMapper, times(2)).map(product)
        assertEquals(result.name, categoryModel.name)
    }
}
