package com.mobcategories.nl.model

import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product
import com.mobcategories.nl.data.SalePrice
import com.mobcategories.nl.data.remote.ImageUrlMapper
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.domain.callLocal
import com.mobcategories.nl.domain.callRemote
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.matchers.Null
import java.io.IOException
import java.lang.NullPointerException
import kotlin.concurrent.timer

@ExperimentalCoroutinesApi
class ProductMapperTest {

    @Mock
    private lateinit var imageUrlMapper: ImageUrlMapper

    @Mock
    private lateinit var salePriceMapper: SalePriceMapper

    private lateinit var productMapper: ProductMapper

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        productMapper = ProductMapper(imageUrlMapper, salePriceMapper)
    }

    @Test
    fun `when call map return proper product model`() = runBlockingTest {
        val imageUrl = "imageUrl"
        val salePrice = SalePrice()

        val product = Product(
            url = imageUrl,
            salePrice = salePrice
        )

        `when`(imageUrlMapper.map(imageUrl)).thenReturn("")
        `when`(salePriceMapper.map(salePrice)).thenReturn("")

        productMapper.map(product)

        verify(imageUrlMapper).map(imageUrl)
        verify(salePriceMapper).map(salePrice)
    }
}
