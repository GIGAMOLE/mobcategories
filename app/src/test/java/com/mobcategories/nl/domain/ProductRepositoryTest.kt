package com.mobcategories.nl.domain

import com.mobcategories.nl.data.Product
import com.mobcategories.nl.data.ProductRepositoryImpl
import com.mobcategories.nl.data.local.ProductDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class ProductRepositoryTest {

    @Mock
    private lateinit var productDao: ProductDao

    private lateinit var productRepository: ProductRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        productRepository =
            ProductRepositoryImpl(productDao)
    }

    @Test
    fun `when get product local return result then success`() = runBlockingTest {
        val id = "id"
        val categoryId = "categoryId"
        val product =
            Product(id = id, categoryId = categoryId)

        `when`(productDao.get(id, categoryId)).thenReturn(product)

        val result = productRepository.getProductLocal(id, categoryId)

        verify(productDao).get(id, categoryId)
        assertEquals(product, result)
    }
}
