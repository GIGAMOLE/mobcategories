package com.mobcategories.nl.domain

import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.CategoryRepositoryImpl
import com.mobcategories.nl.data.Product
import com.mobcategories.nl.data.local.CategoryDao
import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.data.remote.ApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class CategoryRepositoryTest {

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var categoryDao: CategoryDao

    @Mock
    private lateinit var productDao: ProductDao

    private lateinit var categoryRepository: CategoryRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        categoryRepository = CategoryRepositoryImpl(
            apiService,
            categoryDao,
            productDao
        )
    }

    @Test
    fun `when get all categories local return result then success`() = runBlockingTest {
        val id1 = "1"
        val id2 = "2"

        val categories = listOf(
            Category(id = id1),
            Category(id = id2)
        )

        val products1 = listOf(
            Product(
                id = id1,
                categoryId = id1
            )
        )
        val products2 = listOf(
            Product(
                id = id2,
                categoryId = id2
            )
        )

        `when`(categoryDao.getAll()).thenReturn(categories)
        `when`(productDao.getAll(id1)).thenReturn(products1)
        `when`(productDao.getAll(id2)).thenReturn(products2)

        val result = categoryRepository.getAllCategoriesLocal()

        verify(categoryDao).getAll()
        verify(productDao).getAll(id1)
        verify(productDao).getAll(id2)

        Assert.assertEquals(categories, result)
    }

    @Test
    fun `when get all categories remote return result then success`() = runBlockingTest {
        val id1 = "1"
        val id2 = "2"

        val categories = listOf(
            Category(id = id1),
            Category(id = id2)
        )

        `when`(apiService.getAllCategories()).thenReturn(categories)

        val result = categoryRepository.getAllCategoriesRemote()

        verify(apiService).getAllCategories()

        Assert.assertEquals(categories, result)
    }

    @Test
    fun `when save all categories with success`() = runBlockingTest {
        val id1 = "1"
        val id2 = "2"

        val products1 = listOf(
            Product(
                id = id1,
                categoryId = id1
            )
        )
        val products2 = listOf(
            Product(
                id = id2,
                categoryId = id2
            )
        )

        val categories = listOf(
            Category(id = id1, products = products1),
            Category(id = id2, products = products2)
        )

        categoryRepository.saveAllCategories(categories)

        verify(categoryDao).insertAll(categories)
        verify(productDao).insertAll(products1)
        verify(productDao).insertAll(products2)
    }
}
