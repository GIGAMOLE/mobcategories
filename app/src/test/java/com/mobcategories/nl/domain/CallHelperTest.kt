package com.mobcategories.nl.domain

import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.domain.callLocal
import com.mobcategories.nl.domain.callRemote
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.internal.matchers.Null
import java.io.IOException
import java.lang.NullPointerException

@ExperimentalCoroutinesApi
class CallHelperTest {

    private val dispatcher = TestCoroutineDispatcher()

    @Test
    fun `when call local lambda return result then success`() = runBlockingTest {
        val lambdaResult = true
        val result = callLocal(dispatcher) { lambdaResult }
        assertEquals(Result.Success(lambdaResult), result)
    }

    @Test
    fun `when call local lambda return IO exception then db error`() = runBlockingTest {
        val result = callLocal(dispatcher) { throw IOException() }
        assertEquals(Result.DatabaseError, result)
    }

    @Test
    fun `when call local lambda return any exception then generic error`() = runBlockingTest {
        val nullPointerException = NullPointerException()
        val result = callLocal(dispatcher) { throw nullPointerException }
        assertEquals(Result.GenericError(nullPointerException), result)
    }

    @Test
    fun `when call remote lambda return result then success`() = runBlockingTest {
        val lambdaResult = true
        val result = callRemote(dispatcher) { lambdaResult }
        assertEquals(Result.Success(lambdaResult), result)
    }

    @Test
    fun `when call remote lambda return IO exception then network error`() = runBlockingTest {
        val result = callRemote(dispatcher) { throw IOException() }
        assertEquals(Result.NetworkError, result)
    }

    @Test
    fun `when call remote lambda return any exception then generic error`() = runBlockingTest {
        val nullPointerException = NullPointerException()
        val result = callRemote(dispatcher) { throw nullPointerException }
        assertEquals(Result.GenericError(nullPointerException), result)
    }
}
