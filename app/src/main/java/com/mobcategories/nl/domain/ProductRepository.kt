package com.mobcategories.nl.domain

import com.mobcategories.nl.data.Product
import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface ProductRepository {
    suspend fun getProductLocal(id: String, categoryId: String): Product
}
