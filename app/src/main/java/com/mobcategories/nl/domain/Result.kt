package com.mobcategories.nl.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.IOException

sealed class Result<out T> {
    object DatabaseError : Result<Nothing>()
    object NetworkError : Result<Nothing>()
    data class GenericError(val throwable: Throwable) : Result<Nothing>()
    data class Success<out T>(val value: T) : Result<T>()
}

suspend fun <T> callLocal(dispatcher: CoroutineDispatcher, call: suspend () -> T): Result<T> {
    return withContext(dispatcher) {
        try {
            Result.Success(call.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> Result.DatabaseError
                else -> Result.GenericError(throwable)
            }
        }
    }
}

suspend fun <T> callRemote(dispatcher: CoroutineDispatcher, call: suspend () -> T): Result<T> {
    return withContext(dispatcher) {
        try {
            Result.Success(call.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> Result.NetworkError
                else -> Result.GenericError(throwable)
            }
        }
    }
}
