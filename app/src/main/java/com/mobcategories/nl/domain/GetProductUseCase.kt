package com.mobcategories.nl.domain

import com.mobcategories.nl.data.Product
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class GetProductUseCase(
    private val productRepository: ProductRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    suspend fun getProduct(id: String, categoryId: String): Result<Product> =
        callLocal(dispatcher) { productRepository.getProductLocal(id, categoryId) }
}
