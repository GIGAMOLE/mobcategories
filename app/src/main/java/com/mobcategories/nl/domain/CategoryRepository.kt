package com.mobcategories.nl.domain

import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.local.CategoryDao
import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.data.remote.ApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface CategoryRepository {

    suspend fun getAllCategoriesLocal(): List<Category>

    suspend fun getAllCategoriesRemote(): List<Category>

    suspend fun saveAllCategories(categories: List<Category>)
}
