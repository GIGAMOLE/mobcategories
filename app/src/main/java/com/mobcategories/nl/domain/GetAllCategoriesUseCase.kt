package com.mobcategories.nl.domain

import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.remote.NetworkChecker
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class GetAllCategoriesUseCase(
    private val categoryRepository: CategoryRepository,
    private val networkChecker: NetworkChecker,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    suspend fun getAllCategories(): Result<List<Category>> {
        return if (networkChecker.isAvailable()) getAllCategoriesRemote()
        else callLocal(dispatcher) { categoryRepository.getAllCategoriesLocal() }
    }

    private suspend fun getAllCategoriesRemote() = callRemote(dispatcher) {
        categoryRepository.getAllCategoriesRemote().apply {
            categoryRepository.saveAllCategories(this)
        }
    }
}
