package com.mobcategories.nl.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mobcategories.nl.data.*
import org.koin.dsl.module

object DatabaseClient {

    private const val DATABASE_NAME = "database"

    // DB module
    val module = module {
        single { provideDatabase(get()) }
        single { get<MobcategoriesDatabase>().categoryDao() }
        single { get<MobcategoriesDatabase>().productDao() }
    }

    private fun provideDatabase(context: Context) = Room.databaseBuilder(
        context.applicationContext,
        MobcategoriesDatabase::class.java,
        DATABASE_NAME
    ).build()
}
