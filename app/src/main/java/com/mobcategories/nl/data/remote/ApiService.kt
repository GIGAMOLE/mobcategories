package com.mobcategories.nl.data.remote

import com.mobcategories.nl.data.Category
import retrofit2.http.GET

interface ApiService {
    @GET(".")
    suspend fun getAllCategories(): List<Category>
}
