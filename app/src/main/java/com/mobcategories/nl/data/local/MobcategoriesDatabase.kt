package com.mobcategories.nl.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product

@Database(
    entities = [
        Category::class,
        Product::class
    ],
    version = 1,
    exportSchema = false
)
abstract class MobcategoriesDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun productDao(): ProductDao
}
