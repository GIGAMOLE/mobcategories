package com.mobcategories.nl.data

import com.mobcategories.nl.data.local.CategoryDao
import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.data.remote.ApiService
import com.mobcategories.nl.domain.CategoryRepository

class CategoryRepositoryImpl(
    private val apiService: ApiService,
    private val categoryDao: CategoryDao,
    private val productDao: ProductDao
) : CategoryRepository {

    // We could do the same with the @ForeignKey and TypeConverter
    // But again, in order to save time :)
    override suspend fun getAllCategoriesLocal() =
        categoryDao.getAll().apply {
            forEach {
                it.products = productDao.getAll(it.id)
            }
        }

    override suspend fun getAllCategoriesRemote() =
        apiService.getAllCategories()

    override suspend fun saveAllCategories(categories: List<Category>) {
        categoryDao.insertAll(categories)
        categories.forEach {
            productDao.insertAll(it.products)
        }
    }
}
