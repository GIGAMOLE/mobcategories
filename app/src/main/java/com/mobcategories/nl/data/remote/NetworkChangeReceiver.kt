package com.mobcategories.nl.data.remote

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NetworkChangeReceiver(
    private val networkChecker: NetworkChecker? = null
) : BroadcastReceiver() {

    lateinit var onNetworkChanged: (Boolean) -> Unit

    // We are not using any intent-action check, so let's suppress it
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context?, intent: Intent?) {
        networkChecker?.let {
            onNetworkChanged(
                it.isAvailable()
            )
        }
    }
}
