package com.mobcategories.nl.data

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity
data class Category(
    @SerializedName("description")
    var description: String = "",
    @SerializedName("id")
    @PrimaryKey
    var id: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("products")
    @Ignore
    var products: List<Product> = emptyList()
)

@Entity(primaryKeys = ["id", "categoryId"])
data class Product(
    @SerializedName("categoryId")
    var categoryId: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("salePrice")
    @Embedded(prefix = "salePrice")
    var salePrice: SalePrice = SalePrice("", ""),
    @SerializedName("url")
    var url: String = ""
)

data class SalePrice(
    @SerializedName("amount")
    var amount: String = "",
    @SerializedName("currency")
    var currency: String = ""
)
