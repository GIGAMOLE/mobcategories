package com.mobcategories.nl.data

import com.mobcategories.nl.data.local.ProductDao
import com.mobcategories.nl.domain.ProductRepository

class ProductRepositoryImpl(
    private val productDao: ProductDao
) : ProductRepository {
    override suspend fun getProductLocal(id: String, categoryId: String) =
        productDao.get(id, categoryId)
}
