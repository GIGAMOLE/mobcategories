package com.mobcategories.nl.data.remote

class ImageUrlMapper(private val baseUrl: String) {
    fun map(imageUrl: String) = baseUrl + imageUrl
}
