package com.mobcategories.nl.data.remote

interface NetworkChecker {
    fun isAvailable(): Boolean
}
