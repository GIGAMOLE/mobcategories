package com.mobcategories.nl.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insertAll(categories: List<Product>)

    @Query("SELECT * from Product WHERE categoryId = :categoryId")
    suspend fun getAll(categoryId: String): List<Product>

    @Query("SELECT * from Product WHERE id = :id AND categoryId = :categoryId")
    suspend fun get(id: String, categoryId: String): Product
}
