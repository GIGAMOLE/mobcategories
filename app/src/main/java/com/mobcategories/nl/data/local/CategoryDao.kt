package com.mobcategories.nl.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mobcategories.nl.data.Category

@Dao
interface CategoryDao {

    @Query("SELECT * from Category")
    suspend fun getAll(): List<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insertAll(categories: List<Category>)
}
