package com.mobcategories.nl.app

import android.app.Application
import coil.Coil
import coil.ImageLoader
import coil.util.CoilUtils
import com.mobcategories.nl.data.CategoryRepositoryImpl
import com.mobcategories.nl.data.ProductRepositoryImpl
import com.mobcategories.nl.data.local.DatabaseClient
import com.mobcategories.nl.data.remote.ApiClient
import com.mobcategories.nl.data.remote.NetworkChangeReceiver
import com.mobcategories.nl.domain.*
import com.mobcategories.nl.model.CategoryMapper
import com.mobcategories.nl.model.ProductMapper
import com.mobcategories.nl.model.SalePriceMapper
import com.mobcategories.nl.view.details.DetailsViewModel
import com.mobcategories.nl.view.list.ListViewModel
import com.mobcategories.nl.view.main.MainViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

@Suppress("unused")
class MobcategoriesApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MobcategoriesApp)
            modules(
                listOf(
                    module,
                    ApiClient.module,
                    DatabaseClient.module
                )
            )
        }

        Coil.setImageLoader(
            ImageLoader.Builder(this)
                .crossfade(true)
                .okHttpClient {
                    OkHttpClient.Builder()
                        .cache(CoilUtils.createDefaultCache(this@MobcategoriesApp))
                        .build()
                }
                .build()
        )
    }

    // App module
    private val module = module {
        viewModel { MainViewModel() }
        viewModel { ListViewModel(get(), get()) }
        viewModel { DetailsViewModel(get(), get()) }

        factory { CategoryMapper(get()) }
        factory { ProductMapper(get(), get()) }
        factory { SalePriceMapper() }

        factory<CategoryRepository> {
            CategoryRepositoryImpl(
                get(),
                get(),
                get()
            )
        }
        factory<ProductRepository> {
            ProductRepositoryImpl(
                get()
            )
        }

        factory { NetworkChangeReceiver(get()) }
        factory { GetAllCategoriesUseCase(get(), get()) }
        factory { GetProductUseCase(get()) }
    }
}
