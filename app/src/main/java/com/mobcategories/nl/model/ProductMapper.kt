package com.mobcategories.nl.model

import com.mobcategories.nl.data.Product
import com.mobcategories.nl.data.remote.ImageUrlMapper

class ProductMapper(
    private val imageUrlMapper: ImageUrlMapper,
    private val salePriceMapper: SalePriceMapper
) {
    fun map(product: Product) = ProductModel(
        id = product.id,
        categoryId = product.categoryId,
        name = product.name,
        imageUrl = imageUrlMapper.map(product.url),
        priceFormat = salePriceMapper.map(product.salePrice)
    )
}
