package com.mobcategories.nl.model

import java.io.Serializable

data class CategoryModel(
    val name: String,
    val products: List<ProductModel>
) : Serializable

data class ProductModel(
    val id: String,
    val categoryId: String,
    val name: String,
    val imageUrl: String,
    val priceFormat: String
) : Serializable
