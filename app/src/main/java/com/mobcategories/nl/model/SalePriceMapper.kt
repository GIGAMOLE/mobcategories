package com.mobcategories.nl.model

import com.mobcategories.nl.data.SalePrice

class SalePriceMapper {
    fun map(salePrice: SalePrice): String {
        return "${salePrice.amount} ${salePrice.currency}"
    }
}
