package com.mobcategories.nl.model

import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product

class CategoryMapper(private val productMapper: ProductMapper) {

    fun map(category: Category) = CategoryModel(
        name = category.name,
        products = category.products.map { productMapper.map(it) }
    )

    fun map(categories: List<Category>) = categories.map { map(it) }
}
