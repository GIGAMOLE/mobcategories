package com.mobcategories.nl.view.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobcategories.nl.data.Category
import com.mobcategories.nl.domain.GetAllCategoriesUseCase
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.model.CategoryMapper
import com.mobcategories.nl.model.CategoryModel
import com.mobcategories.nl.view.main.ModelState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class ListViewModel(
    private val getAllCategoriesUseCase: GetAllCategoriesUseCase,
    private val categoryMapper: CategoryMapper
) : ViewModel() {

    private val _allCategories = MutableLiveData<List<CategoryModel>>()
    val allCategories: LiveData<List<CategoryModel>> = _allCategories

    private val _allCategoriesState = MutableLiveData<ModelState>()
    val allCategoriesState: LiveData<ModelState> = _allCategoriesState

    init {
        getAllCategories()
    }

    fun getAllCategories() {
        _allCategoriesState.postValue(ModelState.LOADING)
        viewModelScope.launch(CoroutineExceptionHandler { _, _ ->
            _allCategoriesState.postValue(ModelState.GENERIC_ERROR)
        }) {
            when (val result = getAllCategoriesUseCase.getAllCategories()) {
                is Result.NetworkError -> _allCategoriesState.postValue(ModelState.NETWORK_ERROR)
                is Result.DatabaseError -> _allCategoriesState.postValue(ModelState.DATABASE_ERROR)
                is Result.GenericError -> _allCategoriesState.postValue(ModelState.GENERIC_ERROR)
                is Result.Success -> {
                    if (result.value.isEmpty()) {
                        _allCategoriesState.postValue(ModelState.EMPTY)
                    } else {
                        _allCategories.postValue(
                            categoryMapper.map(result.value)
                        )
                        _allCategoriesState.postValue(ModelState.SUCCESS)
                    }
                }
            }
        }
    }
}
