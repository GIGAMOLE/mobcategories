package com.mobcategories.nl.view.details

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import coil.api.load
import com.mobcategories.nl.R
import com.mobcategories.nl.model.ProductModel
import com.mobcategories.nl.view.main.MainViewModel
import com.mobcategories.nl.view.main.ModelState
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.jvm.internal.impl.renderer.ClassifierNamePolicy


class DetailsFragment : Fragment(R.layout.fragment_details) {

    private val detailsViewModel: DetailsViewModel by viewModel()

    private val ids by lazy {
        arguments?.let {
            DetailsFragmentArgs.fromBundle(it).id to
                    DetailsFragmentArgs.fromBundle(it).categoryId
        } ?: "" to ""
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupUI()

        reload()
    }

    private fun setupViewModel() {
        detailsViewModel.product.observe(viewLifecycleOwner, Observer { model ->
            setupProductModel(model)
        })
        detailsViewModel.productState.observe(viewLifecycleOwner, Observer { state ->
            handleInfoMessage(state)
        })
    }

    private fun setupUI() {
        detailsBtnBack.setOnClickListener { back() }
    }

    private fun setupProductModel(model: ProductModel) {
        detailsImg.load(model.imageUrl)

        detailsTxtName.text = model.name
        detailsTxtPrice.text = model.priceFormat

    }

    private fun reload() = detailsViewModel.getProduct(ids.first, ids.second)

    private fun handleInfoMessage(state: ModelState) {
        Toast.makeText(
            requireContext(),
            when (state) {
                ModelState.NETWORK_ERROR -> R.string.state_network_error
                ModelState.DATABASE_ERROR -> R.string.state_database_error
                ModelState.GENERIC_ERROR -> R.string.state_error
                ModelState.EMPTY -> R.string.state_empty
                else -> return
            },
            Toast.LENGTH_SHORT
        ).show()

        back()
    }

    private fun back() = findNavController().navigateUp()
}
