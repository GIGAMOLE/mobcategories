package com.mobcategories.nl.view.main

enum class ModelState {
    LOADING, NETWORK_ERROR, DATABASE_ERROR, GENERIC_ERROR, EMPTY, SUCCESS
}
