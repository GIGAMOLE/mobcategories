package com.mobcategories.nl.view.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobcategories.nl.data.Category
import com.mobcategories.nl.data.Product
import com.mobcategories.nl.domain.GetAllCategoriesUseCase
import com.mobcategories.nl.domain.GetProductUseCase
import com.mobcategories.nl.domain.Result
import com.mobcategories.nl.model.CategoryMapper
import com.mobcategories.nl.model.CategoryModel
import com.mobcategories.nl.model.ProductMapper
import com.mobcategories.nl.model.ProductModel
import com.mobcategories.nl.view.main.ModelState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class DetailsViewModel(
    private val getProductUseCase: GetProductUseCase,
    private val productMapper: ProductMapper
) : ViewModel() {

    private val _product = MutableLiveData<ProductModel>()
    val product: LiveData<ProductModel> = _product

    private val _productState = MutableLiveData<ModelState>()
    val productState: LiveData<ModelState> = _productState

    fun getProduct(id: String, categoryId: String) {
        _productState.postValue(ModelState.LOADING)
        viewModelScope.launch(CoroutineExceptionHandler { _, _ ->
            _productState.postValue(ModelState.GENERIC_ERROR)
        }) {
            when (val result = getProductUseCase.getProduct(id, categoryId)) {
                is Result.NetworkError -> _productState.postValue(ModelState.NETWORK_ERROR)
                is Result.DatabaseError -> _productState.postValue(ModelState.DATABASE_ERROR)
                is Result.GenericError -> _productState.postValue(ModelState.GENERIC_ERROR)
                is Result.Success -> {
                    _product.postValue(
                        productMapper.map(result.value)
                    )
                    _productState.postValue(ModelState.SUCCESS)
                }
            }
        }
    }
}
