package com.mobcategories.nl.view.list

import android.view.View
import android.widget.ImageView
import coil.api.load
import com.chad.library.adapter.base.BaseSectionQuickAdapter
import com.chad.library.adapter.base.entity.SectionEntity
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.mobcategories.nl.R
import com.mobcategories.nl.model.CategoryModel
import com.mobcategories.nl.model.ProductModel

abstract class CategoriesAdapter :
    BaseSectionQuickAdapter<CategoriesAdapter.CategorySection, BaseViewHolder>(
        R.layout.item_category_section,
        arrayListOf()
    ), View.OnClickListener {

    init {
        setNormalLayout(R.layout.item_category)
    }

    fun setData(newData: List<CategoryModel>) {
        data.clear()
        newData.forEach {
            data.add(CategorySection(it.name))
            it.products.forEach { model ->
                data.add(CategorySection(model))
            }
        }

        notifyDataSetChanged()
    }

    override fun convertHeader(helper: BaseViewHolder, item: CategorySection) {
        helper.setText(R.id.itemCategorySectionTxt, item.sectionName)
    }

    override fun convert(holder: BaseViewHolder, item: CategorySection) {
        holder.getView<ImageView>(R.id.itemCategoryImg).load(item.model?.imageUrl)

        holder.setText(R.id.itemCategoryTxtName, item.model?.name)
        holder.setText(R.id.itemCategoryTxtPrice, item.model?.priceFormat)

        val itemView = holder.getView<View>(R.id.itemCategory)
        itemView.setOnClickListener(this)
        itemView.tag = item.model
    }

    override fun onClick(v: View?) = onItemClick(v?.tag as ProductModel)

    abstract fun onItemClick(model: ProductModel)

    inner class CategorySection private constructor(
        override val isHeader: Boolean
    ) : SectionEntity {
        var model: ProductModel? = null
        var sectionName: String? = null

        constructor(model: ProductModel) : this(false) {
            this.model = model
        }

        constructor(sectionName: String) : this(true) {
            this.sectionName = sectionName
        }
    }
}
