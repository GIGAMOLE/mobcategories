package com.mobcategories.nl.view.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mobcategories.nl.R
import com.mobcategories.nl.model.ProductModel
import com.mobcategories.nl.view.main.MainViewModel
import com.mobcategories.nl.view.main.ModelState
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ListFragment : Fragment(R.layout.fragment_list) {

    private val mainViewModel: MainViewModel by sharedViewModel()
    private val listViewModel: ListViewModel by viewModel()

    private lateinit var categoriesAdapter: CategoriesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupUI()
    }

    private fun setupViewModel() {
        mainViewModel.onBecomeOnline.observe(viewLifecycleOwner, Observer {
            reload()
        })

        listViewModel.allCategories.observe(viewLifecycleOwner, Observer { data ->
            categoriesAdapter.setData(data)
        })
        listViewModel.allCategoriesState.observe(viewLifecycleOwner, Observer { state ->
            setState(state)
            handleInfoMessage(state)
        })
    }

    private fun setupUI() {
        categoriesAdapter = object : CategoriesAdapter() {
            override fun onItemClick(model: ProductModel) {
                openDetails(model)
            }
        }

        listCategories.setHasFixedSize(true)
        listCategories.adapter = categoriesAdapter

        listErrorBtnReload.setOnClickListener { reload() }
    }

    private fun openDetails(model: ProductModel) {
        findNavController().navigate(
            ListFragmentDirections.toDetailsFragment(
                id = model.id,
                categoryId = model.categoryId
            )
        )
    }

    private fun reload() = listViewModel.getAllCategories()

    private fun setState(state: ModelState) {
        val index = when (state) {
            ModelState.LOADING -> 0
            ModelState.SUCCESS -> 2
            else -> 1
        }

        if (listState.displayedChild == index) return
        listState.displayedChild = index
    }

    private fun handleInfoMessage(state: ModelState) {
        when (state) {
            ModelState.NETWORK_ERROR -> listErrorTxt.setText(R.string.state_network_error)
            ModelState.DATABASE_ERROR -> listErrorTxt.setText(R.string.state_database_error)
            ModelState.GENERIC_ERROR -> listErrorTxt.setText(R.string.state_error)
            ModelState.EMPTY -> listErrorTxt.setText(R.string.state_empty)
            else -> return
        }
    }
}
