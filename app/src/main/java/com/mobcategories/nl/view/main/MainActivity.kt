package com.mobcategories.nl.view.main

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.mobcategories.nl.R
import com.mobcategories.nl.data.remote.NetworkChangeReceiver
import com.mobcategories.nl.view.list.ListFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.compat.SharedViewModelCompat.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val mainViewModel: MainViewModel by viewModel()
    private val networkChangeReceiver: NetworkChangeReceiver by inject()

    private lateinit var offlineSnackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup()
    }

    override fun onStart() {
        registerNetworkChangeReceiver()
        super.onStart()
    }

    override fun onStop() {
        unregisterNetworkChangeReceiver()
        super.onStop()
    }

    private fun setup() {
        offlineSnackbar = Snackbar.make(
            mainCoordinator, R.string.error_no_network, Snackbar.LENGTH_INDEFINITE
        ).apply {
            setBackgroundTint(
                ContextCompat.getColor(this@MainActivity, R.color.offline)
            )
        }
        networkChangeReceiver.onNetworkChanged = callback@ ({ isAvailable ->
            if (!isAvailable && offlineSnackbar.isShown) return@callback


            if (isAvailable) {
                if (offlineSnackbar.isShown) {
                    mainViewModel.triggerOnBecomeOnline()
                }

                offlineSnackbar.dismiss()
            } else offlineSnackbar.show()
        })
    }

    // We can use registerNetworkCallback() from ConnectivityManager also
    @Suppress("DEPRECATION")
    private fun registerNetworkChangeReceiver() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) registerReceiver(
            networkChangeReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    private fun unregisterNetworkChangeReceiver() {
        try {
            unregisterReceiver(networkChangeReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }
}
