package com.mobcategories.nl.view.main

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private val _onBecomeOnline = MutableLiveData<Unit>()
    val onBecomeOnline: LiveData<Unit> = _onBecomeOnline

    fun triggerOnBecomeOnline() = _onBecomeOnline.postValue(Unit)
}
