# Mobcategories

All the application structure has done from scratch

This project was written by using modern way of Android Development. I used MVVM/Clean Architecture, Koil, Koin, BRVAH, Room. This application has a dynamic network listener and offline mode.

In order to save the time, the true `Domain Layer` was skipped, so the communication goes directly from the `UI` to the `Data`

There is only functional testing coverage, you can find them in the following folder `/app/src/test`

I have created another branch `/clean-arch` (not working), that demonstrate some multi module approach

Have a nice day :)

## Preview

List:  
![](/gifs/list.gif)

Details:  
![](/gifs/details.gif)

Offline:  
![](/gifs/offline.gif)

## Author

[Basil Miller](https://www.linkedin.com/in/gigamole/)  
[+380 50 569 8419](tel:380505698419)  
[gigamole53@gmail.com](mailto:gigamole53@gmail.com)
